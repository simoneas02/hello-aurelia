# `hello-aurelia`

## Run dev app

Run `npm start`, then open `http://localhost:8080`

To enable Webpack Bundle Analyzer, do `npm run analyze` (production build).

To enable hot module reload, do `npm start -- --hmr`.

To change dev server port, do `npm start -- --port 8888`.

To change dev server host, do `npm start -- --host 127.0.0.1`

**PS:** You could mix all the flags as well, `npm start -- --host 127.0.0.1 --port 7070 --open --hmr`

## Build for production

Run `npm run build`

## Unit tests

Run `npm run test`
