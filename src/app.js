import { PLATFORM } from "aurelia-framework";

export class App {
  message = "Hello Aurelia!";

  configureRouter(config, router) {
    config.title = this.message;
    config.map([
      {
        route: [""],
        name: "home",
        moduleId: PLATFORM.moduleName("components/posts/posts"),
        title: "All Posts",
      },
      {
        route: ["post/:slug"],
        name: "post-view",
        moduleId: PLATFORM.moduleName("components/posts/post-view/post-view"),
        title: "Post View",
      },
      {
        route: ["todo"],
        name: "todo",
        moduleId: PLATFORM.moduleName("components/todo/todo"),
        title: "Todo",
      },
    ]);

    this.router = router;
  }
}
