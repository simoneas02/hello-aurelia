import { inject } from "aurelia-framework";

import { PostService } from "../../common/services/postService";

@inject(PostService)
export class Posts {
  title = "All Posts!";

  constructor(Post) {
    this.postService = Post;
  }

  attached() {
    this.error = "";

    this.postService
      .allPostPreviews()
      .then((data) => {
        this.posts = data.posts;
      })
      .catch((error) => {
        this.error = error.message;
      });
  }
}
