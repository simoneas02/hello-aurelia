import { inject } from "aurelia-framework";

import { PostService } from "../../../common/services/postService";

@inject(PostService)
export class PostView {
  constructor(Post) {
    this.postService = Post;
  }

  activate(params) {
    this.error = "";

    this.postService
      .find(params)
      .then((data) => {
        this.post = data.post;
      })
      .catch((error) => {
        this.error = error.message;
      });
  }

  attached() {}
}
