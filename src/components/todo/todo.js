export class Todo {
  constructor(description) {
    this.description = description;
    this.done = false;
    this.newItem = "";

    this.todoList = [];
    // this.todoList = [...this.todoList, new Todo("First task!")];
    // this.todoList = [...this.todoList, new Todo("Second task!")];
    // this.todoList = [...this.todoList, new Todo("Third task!")];
  }

  addItem() {
    this.todoList = [...this.todoList, new Todo(this.newItem)];
    this.newItem = "";
  }

  removeTodo(todo) {
    this.todoList = this.todoList.filter((item) => item !== todo);
  }
}
