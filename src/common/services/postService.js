import { inject } from "aurelia-framework";

import { months, posts } from "../data/posts";
import { AuthService } from "./authService";

@inject(AuthService)
export class PostService {
  constructor(Auth) {
    this.auth = Auth;
    this.delay = 100;

    if (!this.posts) {
      this.posts = posts;
    }
  }

  allPostPreviews() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (!this.posts) {
          reject(new Error("There was an error retrieving the posts."));
        }

        let previous = this.posts.map(
          ({ title, body, slug, tags, author, createdAt }) => ({
            title,
            body: body.substring(0, 200) + "...",
            slug,
            tags,
            author,
            createdAt,
          })
        );

        previous.sort((a, b) => b.createdAt - a.createdAt);

        resolve({ posts: previous });
      }, this.delay);
    });
  }

  allArchives() {
    return new Promise((resolve, reject) => {
      let archives = [];

      this.posts.sort((a, b) => b.createdAt - a.createdAt);

      this.posts.forEach((post) => {
        archives = [
          ...archives,
          `${
            months[post.createdAt.getMonth()]
          } ${post.createdAt.getFullYear()}`,
        ];
      });

      if (!archives) {
        return resolve({
          error: "There was an error retrieving the archives.",
        });
      }

      resolve({
        archives: archives.filter(
          (archive, index, array) => array.indexOf[archive] === index
        ),
      });
    });
  }

  allTags() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let tags = [];

        this.posts.forEach((post) => {
          tags = tags.concat(post.tags);
        });

        if (!tags) {
          reject(new Error("There was an error retrieving the tags."));
        }

        resolve({
          tags: tags.filter(
            (tag, index, array) => array.indexOf[tag] === index
          ),
        });
      }, this.delay);
    });
  }

  create(post) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let currentUser = this.currentuser;

        if (!currentUser) {
          resolve({ error: "You must be logged in to create a post." });
        }

        let slug = this.sluggify(post.title);

        const newPost = {
          title: post.title,
          body: post.body,
          author: currentUser,
          slug,
          tags: post.tags,
          createdAt: post.createdAt,
        };

        this.posts = [...this.posts, newPost];

        resolve({ slug });
      }, this.delay);
    });
  }

  find({ slug }) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const post = this.posts.find(
          (p) => p.slug.toLowerCase() === slug.toLowerCase()
        );

        if (!post) {
          reject(new Error("Post not found."));
        }

        resolve({ post });
      }, this.delay);
    });
  }

  postsByTag(tag) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (!this.posts) {
          reject(new Error("Error finding posts."));
        }

        resolve({
          posts: this.posts
            .filter((post) => post.tags.includes(tag))
            .sort((a, b) => b.createdAt - a.createdAt),
        });
      }, this.delay);
    });
  }

  postsByArchive(archive) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (!this.posts) {
          reject(new Error("Error finding posts."));
        }

        resolve({
          posts: this.posts
            .filter(
              (post) =>
                archive ===
                `${
                  months[post.createdAt.getMonth()]
                } ${post.createdAt.getFullYear()}`
            )
            .sort((a, b) => b.createdAt - a.createdAt),
        });
      }, this.delay);
    });
  }

  slugify(text) {
    return text
      .toString()
      .toLowerCase()
      .replace(/\s+/g, "-")
      .replace(/[^\w-]+/g, "")
      .replace(/--+/g, "-")
      .replace(/^-+/, "")
      .replace(/-+$/, "");
  }

  update(newPost) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let toUpdate = this.posts.find(
          (post) =>
            post.slug === newPost.slug &&
            post.author === this.authService.currentUser
        );

        if (!toUpdate) {
          reject(new Error("There was an error updating the post."));
        }

        toUpdate = newPost;
        resolve({ slug: toUpdate.slug });
      }, this.delay);
    });
  }
}
