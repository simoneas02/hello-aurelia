export class AuthService {
  constructor() {
    this.delay = 100;
    this.currentUser = null;
    this.users = ["Simone Amorim", "Diego Amorim"];
  }

  login(name) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (!this.users.includes(name)) {
          reject(new Error("Invalid credentials."));
        }

        this.currentUser = name;
        resolve({ user: name });
      }, this.delay);
    });
  }

  logout() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (this.currentUser) {
          reject(new Error("Error login out."));
        }

        resolve({ success: true });
      }, this.delay);
    });
  }

  signup(name) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (this.users.includes(name)) {
          reject(new Error("This user already exists."));
        }

        this.users = [...this.users, name];
        this.currentUser = name;
        resolve({ user: name });
      }, this.delay);
    });
  }
}
